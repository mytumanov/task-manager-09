package ru.mtumanov.tm.component;

import java.util.Scanner;

import ru.mtumanov.tm.api.ICommandController;
import ru.mtumanov.tm.api.ICommandRepository;
import ru.mtumanov.tm.api.ICommandService;
import ru.mtumanov.tm.constant.ArgumentConstant;
import ru.mtumanov.tm.constant.CommandConstant;
import ru.mtumanov.tm.controller.CommandController;
import ru.mtumanov.tm.repository.CommandRepository;
import ru.mtumanov.tm.service.CommandService;

public final class Bootstrap {
    
    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void start(String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConstant.CMD_ABOUT:
                commandController.showAbout();
            break;
            case ArgumentConstant.CMD_HELP:
                commandController.showHelp();
            break;
            case ArgumentConstant.CMD_VERSION:
                commandController.showVersion();
            break;
            case ArgumentConstant.CMD_INFO:
                commandController.showInfo();
                break;
            case ArgumentConstant.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConstant.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showArguemntError();
            break;
        }
    }

    private void processCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case CommandConstant.CMD_ABOUT:
                commandController.showAbout();
                break;
            case CommandConstant.CMD_HELP:
                commandController.showHelp();
                break;
            case CommandConstant.CMD_VERSION:
                commandController.showVersion();
                break;
            case CommandConstant.CMD_INFO:
                commandController.showInfo();
                break;
            case CommandConstant.CMD_EXIT:
                commandController.exit();
                break;
            case CommandConstant.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case CommandConstant.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showCommandError();
                break;
        }
    }

}
