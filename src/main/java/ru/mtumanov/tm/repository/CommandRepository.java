package ru.mtumanov.tm.repository;

import ru.mtumanov.tm.api.ICommandRepository;
import ru.mtumanov.tm.constant.ArgumentConstant;
import ru.mtumanov.tm.constant.CommandConstant;
import ru.mtumanov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {
    
    public static final Command CMD_HELP = new Command(CommandConstant.CMD_HELP, ArgumentConstant.CMD_HELP, "Show list arguments");

    public static final Command CMD_VERSION = new Command(CommandConstant.CMD_VERSION, ArgumentConstant.CMD_VERSION, "Show program version");

    public static final Command CMD_ABOUT = new Command(CommandConstant.CMD_ABOUT, ArgumentConstant.CMD_ABOUT, "Show about program");

    public static final Command CMD_INFO = new Command(CommandConstant.CMD_INFO, ArgumentConstant.CMD_INFO, "Show system info");

    public static final Command CMD_EXIT = new Command(CommandConstant.CMD_EXIT, null, "Close application");

    public static final Command CMD_COMMANDS = new Command(CommandConstant.CMD_COMMANDS, ArgumentConstant.CMD_COMMANDS, "Show command list");

    public static final Command CMD_ARGUMENTS = new Command(CommandConstant.CMD_ARGUMENTS, ArgumentConstant.CMD_ARGUMENTS, "Show argument list");

    public static final Command[] TERMINAL_COMMANDS = new Command[] {CMD_HELP, CMD_VERSION, CMD_ABOUT, CMD_INFO, CMD_COMMANDS, CMD_ARGUMENTS, CMD_EXIT};

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
